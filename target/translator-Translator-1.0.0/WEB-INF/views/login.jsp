<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="false" %>
<html>
<head>
	<title>Login</title>
	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/translator.css" />" />
</head>
<body align="center">

	<!-- Links -->
	<spring:url value="/user/registration" var="url"></spring:url>
	<a href="${url}">Registration with spring.tld</a>
	
	<!-- Error: Username already exists -->
	<p>${usernameError}</p>
	<p>${error}</p>

	<!-- Languages -->
	<p><a href="?lang=en">English</a><br/><a href="?lang=fr">French</a></p>
	
	<!-- Login Form -->
	<c:url value="/j_spring_security_check" var="loginUrl"/>
    <form action="${loginUrl}" method="post">
	<c:if test="${param.error != null}">
		<p>
			Invalid username and password.
		</p>
	</c:if>
	<c:if test="${param.logout != null}">
		<p>
			You have been logged out.
		</p>
	</c:if>
	<p>
		<label for="username">Username</label>
		<input type="text" id="username" name="username"/>
	</p>
	<p>
		<label for="password">Password</label>
		<input type="password" id="password" name="password"/>
	</p>
	<input type="hidden"
		name="${_csrf.parameterName}"
		value="${_csrf.token}"/>
	<button type="submit" class="btn">Log in</button>
</form>
</body>
</html>