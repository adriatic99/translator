<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Display text</title>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/translator.css" />" />
<script src="<c:url value="/resources/js/jquery-2.1.3.js" />"></script>
<script type="text/javascript">
	$(document).ready(function(){
	})
</script>
</head>
<body>

<p>
${project.name}
<br/>
${username}
<br/>
	<spring:url value="/insert?projectid=${project.projectID}" var="url"></spring:url>
	<a href="${url}">Insert New Text</a>
<br/>
	<spring:url value="/projects" var="url10"></spring:url>
	<a href="${url10}">Projects</a>
<br/>
</p>
----------------------------------------------
<br/>

<!-- Form Search for text -->
<form:form action="search" method="post" commandName="searchform">
	Languages:
	<form:select path="langid" items="${lans}"/><br/>
	Types:
	<form:checkboxes path="typesid" items="${types}" /><br/>
	<form:errors path="typesid" />
	<form:input path="projectid" type="hidden" value="${project.projectID}" />
	<input type="submit" value="Search" />
</form:form>
<hr/>
${red}
<c:forEach var="text" items="${result}">
	<table>
		<tr>
			<td>${text.textID}</td>
		</tr>
		<tr>
			<td>${text.title}</td>
		</tr>
		<tr>
			<td>${text.text}</td>
		</tr>
		<tr>
			<td>${text.link}</td>
		</tr>
	</table>
</c:forEach>

${error}
</body>
</html>