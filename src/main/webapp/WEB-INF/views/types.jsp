<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/translator.css" />" />
</head>
<body>

<a href="${pageContext.request.contextPath}/insert?projectid=${project}">Insert Text</a><br/>
<a href="${pageContext.request.contextPath}/text/display?projectid=${projectid}">Display Text</a><br/>
<a href="${pageContext.request.contextPath}/projects">List of Projects</a><br/>
<hr/>
	<div>
		<form:form action="create" method="post" commandName="typeform">
				<form:input path="name" /><br/>
				<form:errors path="name" cssStyle="color: #ff0000;" />
				<form:input path="projectid" type="hidden" value="${projectid}" />
				<input type="submit" value="Save Type" />
			</form:form>
		</div>
		<br/>
		<hr/>
		<c:forEach var="type" items='${list}'>
			<c:out value="${type.name}" /><br/>
		</c:forEach>
	<br/>code:${code}
	<br/>defaultmessage:${defaultmessage}
	<br/>field:${field}
	<br/>rejectedvalue:${reject}
	<br/>here are values from Error object
</body>
</html>