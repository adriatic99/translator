<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/translator.css" />" />
<script src="<c:url value="/resources/js/jquery-2.1.3.js" />"></script>
<script type="text/javascript">
	$(document).ready(function(){
		
		/*
		$("#w1").change(function() {
			var data = $("#w1").val();
			$.get("word/find", {text: data},function(result) {
				alert();
				$.each(result, function(index) {
		            alert(result[index].wordID);
		            alert(result[index].text);
		            addRadio(result[index].wordID,result[index].translation);
		        });
				//addRadio(result.wordID,result.translation);
		});
			*/
		
		$("#addword").click(function() {
			var id = $('input[name=radio1]').filter(':checked').val();
			if(id==null)
			{
				alert("select one translation or add new translation");
				return;
			}
			var name = $("#w1").val();
			$("#rad").empty();
			addCheckbox(id,name);
			$("#addword").hide();
		});
			
		$("#a").click(function() {
			if(!$("#w1").val())
    		{
    			alert("You need to enter a word in the field");
    			return;
    		}
			
			var data = $("#w1").val();
			var data1 = new Array();
			$("#rad").empty();
			$.ajax({
    			headers: { 
    		        'Accept': 'application/json',
    		        'Content-Type': 'application/json' 
    		    },
    			type: "GET",
    			url: "word/find",
    			data: "text="+data,
    	        dataType: "json"
    		}).done(function(data1) {w1Change(data1);});
		});
			
    	$("#wordsubmit").click(function() {
    		if(!$("#w1").val())
    		{
    			alert("there is no words to add!");
    			return;
    		}
    		if(!$("#w2").val())
    		{
    			alert("You need to add some translation");
    			return;
    		}
    		
    		var param1 = "name:"+$("#w1").val();
    		var param2 = "translation:"+$("#w2").val();
    		var paramPost = "{"+param1 + ", " + param2+"}";
    		var word1 = {"name": "word2", "translation": "trans2"};
    		var paramJSON = JSON.stringify(paramPost);
    		var wordObject = new Object();
    		wordObject.name = $("#w1").val();
    		wordObject.translation = $("#w2").val();
    		var wordToSend = JSON.stringify(wordObject);
    		//paramJSON = "name:"+"word1","translation:"+"translation1";

    		
    		$.post("word/create", wordToSend, function(result){
    			addCheckbox(result.text,result.translation);
    		},'json');
    		$.ajax({
    			headers: { 
    		        'Accept': 'application/json',
    		        'Content-Type': 'application/json' 
    		    },
    			type: "POST",
    			url: "word/create",
    			data: wordToSend,
    	        dataType: "json",
    	        success: function(result){
        			addCheckbox(result.wordID,result.word);
        		}
    		});
    		//alert($('#w1').val());
    		//alert(JSON.stringify(formStr));
    	});
    	
    	function w1Change(result)
    	{
    		if(result.length==0)
    		{
    			alert("There is no translation for this word");
    			return;
    		}
    		var container = $('#rad');
    		
    		$.each(result,function(index,value) {
	            addRadio(result[index].wordID,result[index].translation);
	        });
    		$("#addword").show();
    	}
    	
    	function addRadio(id, translation)
    	{
    		var container = $('#rad');
    		
    		$('<input />', {type: 'radio', name: 'radio1', value: id}).appendTo(container);
    		$('<label />', { text: translation }).appendTo(container);
    		$('<br/>').appendTo(container);
    	}
    	
    	function addCheckbox(id1,name1) {
    	   var container = $('#cb');
    	   var inputs = container.find('input');
    	   var id = inputs.length+1;

    	   $('<input />', { type: 'checkbox', id: 'wordcheckbox'+id, value: id1, name: 'words', checked: 'checked' }).appendTo(container);
    	   $('<label />', { 'for': 'wordcheckbox'+id, text: name1 }).appendTo(container);
    	}
    	
	});
</script>
</head>
<body>
<nav>
<h4>Select Language</h4><br/>
<table>
	<tr>
		<td width="15%"><a href="?projectid=${project.projectID}&lang=en"><spring:message code="master.en" /></a></td>
		<td width="15%"><a href="?projectid=${project.projectID}&lang=es"><spring:message code="master.es" /></a></td>
		<td width="15%"><a href="?projectid=${project.projectID}&lang=pt"><spring:message code="master.pt" /></a></td>
		<td width="15%"><a href="?projectid=${project.projectID}&lang=nl"><spring:message code="master.nl" /></a></td>
		<td width="15%"><a href="?projectid=${project.projectID}&lang=fr"><spring:message code="master.fr" /></a></td>
		<td width="15%"><a href="?projectid=${project.projectID}&lang=it"><spring:message code="master.it" /></a></td>
		<td width="15%"><a href="?projectid=${project.projectID}&lang=de"><spring:message code="master.de" /></a></td>
	</tr>
</table>
</nav>
<p>
Project: ${project.name}
<br/>
User: ${username}
<br/>
	<spring:url value="/text/display?projectid=${project.projectID}" var="url"></spring:url>
	<a href="${url}">Display Text</a>
<br/>
<a href="${pageContext.request.contextPath}/j_spring_security_logout">Logout</a>
</p>
	<hr/>
	<a href="language/new?projectid=${project.projectID}">New Language</a>
	<br/>
	<hr/>
	<a href="type/new?projectid=${project.projectID}">New Type</a>
	<br/>
    <center>--------------------------------------------------------------------------------------</center><br/>
	<hr/>
	Error: ${error}
	<!-- Form -->
	<div id="formtext">
		<form:form commandName="textForm" action="text/save" method="post">
			<table>
				<!-- Text -->
				<tr>
    				<td>Languages:</td>
    				<td><form:select path="languageid" items="${lans}"/></td>
    			</tr>
				<tr>
    				<td>Types:</td>
    				<td><form:checkboxes path="typeids" items="${type}"/></td>
    			</tr>
				<tr>
                    <td></td>
                    <td><form:input path="projectid" type="hidden" value="${project.projectID}" /></td>
                </tr>
				<tr>
					<td>title</td>
					<td><form:input path="title" /></td>
					<form:errors path="title" />
				</tr>
				<tr>
					<td>text</td>
					<td><form:textarea path="text" rows="5" cols="40" /></td>
					<form:errors path="text" />
				</tr>
				<tr>
					<td>link</td>
					<td><form:input path="link" /></td>
					<form:errors path="link" />
				</tr>
				<tr>
					<td>Words added</td>
					<td><div id="cb"><form:checkboxes path="words" items="${cbWords}" itemValue="wordID" itemLabel="word" id="wordcheckbox" element="div" /></div><td>
				</tr>
				<tr>
					<td>submit</td>
					<td><input type="submit" value="Text Form" /></td>
				</tr>
			</table>
		</form:form>
	</div>
	
	<!-- Words -->
	<div id="formword">
		
			<table>
				<tr>
					<td>word</td>
					<td><input type="text" id="w1"/></td>
					
				</tr>
				<tr>
					<td>new translation</td>
					<td><input type="text" id="w2"/></td>
					
				</tr>
				<tr>
					<td></td>
					<td><input type="button" value="Add New Translation to the text" id="wordsubmit" /></td>
				</tr>
			</table>
			<!-- Radio Buttons -->
			<div id="rad"></div>
		
	</div>
	<input type="button" id="a" value="find existing translation for this word" />
	<input type="button" id="addword" value="add existing translation" hidden="hidden" />
</body>
</html>