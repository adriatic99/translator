<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page session="false" %>
<html>
<head>
	<title>Registration Error</title>
	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/translator.css" />" />
</head>
<body>
<h1>
	Registration Error  
</h1>
<a href="<c:url value="j_spring_security_logout" />" >Logout</a><br/>

    <spring:hasBindErrors name="userDAO">
        <h2>Errors</h2>
        <div>
            <ul>
            <c:forEach var="error" items="${errors.allErrors}">
                <li>${error.defaultMessage}</li>
            </c:forEach>
            </ul>
        </div>
    </spring:hasBindErrors>
</body>
</html>