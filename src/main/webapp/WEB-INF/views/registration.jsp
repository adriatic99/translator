<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Registration</title>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/translator.css" />" />
</head>
<body>

	<!-- Links -->
	<spring:url value="/login" var="url"></spring:url>
	<a href="${url}">Login with spring.tld</a>
	
	<!-- Error: Username already exists -->
	<p>${usernameError}</p>
	<p>${error}</p>
	
	<!-- Logout -->
	<p>ContextPath
	<c:out value="${pageContext.request.contextPath}" /></p>
	<p><a href="${pageContext.request.contextPath}/j_spring_security_logout">Logout</a></p>
	
	<!-- Languages -->
	<p><a href="?lang=en">English</a><br/><a href="?lang=fr">French</a></p>
	
	<!-- Registration Form -->
    <div align="center">
        <form:form action="create" method="post" commandName="userForm">
            <table>
                <tr>
                    <td colspan="2" align="center"><h2>Registration</h2></td>
                </tr>
                <tr>
                    <td><spring:message code="login.username" text="uname" /></td>
                    <td><form:input path="username" /></td>
                    <form:errors path="username" element="div" />
                </tr>
                <tr>
                    <td><spring:message code="login.password" text="pwd" /></td>
                    <td><form:password path="password" /></td>
                    <form:errors path="password" />
                </tr>
                <tr>
                    <td><spring:message code="login.password" text="confirm password" /></td>
                    <td><form:password path="confirmPassword" /></td>
                    <form:errors path="confirmPassword" />
                </tr>
                <tr>
                    <td colspan="2" align="center"><input type="submit" value="Register" /></td>
                </tr>
            </table>
        </form:form>
    </div>
    
</body>
</html>