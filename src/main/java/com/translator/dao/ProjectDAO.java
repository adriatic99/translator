package com.translator.dao;

import java.util.List;
import java.util.Set;

import com.translator.domain.Project;
import com.translator.domain.Type;
import com.translator.domain.User;

public interface ProjectDAO<T> extends TranslatorDAO<T> {

	public List<Project> findByUsername(User user);
	public Set<Type> getTypes(int projectID);
}
