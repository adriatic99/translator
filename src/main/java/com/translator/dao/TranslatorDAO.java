package com.translator.dao;

import java.util.List;

public interface TranslatorDAO<T> {
	
	public T findById(int id);     
    public List<T> findAll();      
    public void update(T entity);  
    public void save(T entity);    
    public void delete(T entity);
}
