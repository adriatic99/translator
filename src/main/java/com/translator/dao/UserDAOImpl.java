package com.translator.dao;

import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.translator.domain.Language;
import com.translator.domain.Project;
import com.translator.domain.User;
import com.translator.domain.UserRoles;

@Repository
public class UserDAOImpl<T> implements UserDAO<T> {

	@Autowired
	private SessionFactory session;
	
	public void setSession(SessionFactory session)
	{
		this.session = session;
	}
	
	private Session getCurrentSession()
	{
		return session.getCurrentSession();
	}
	
	@Override
	public T findById(int id) {
		return null;
	}

	@Override
	public List<T> findAll() {
		List<User> users = this.getCurrentSession().createQuery("from User").list();
		return (List<T>) users;
	}

	@Override
	public void update(T entity) {
		User user = (User) entity;
		this.getCurrentSession().update(user);
	}

	@Override
	public void save(T entity) {
		User user = (User) entity;
		this.getCurrentSession().save(user);
	}

	@Override
	public void delete(T entity) {
		User user = (User) entity;
		this.getCurrentSession().delete(user);
	}

	@Override
	public User getUserByUsername(String username) {
		User user = (User) this.getCurrentSession().createQuery("from User where username="+"'"+username+"'").uniqueResult();
		return user;
	}

	@Override
	public List<Project> getProjects(String username) {
		User user = (User) this.getCurrentSession().createQuery("from User where username="+"'"+username+"'").uniqueResult();
		List<Project> projects = (List<Project>) user.getProjects();
		return projects;
	}

	@Override
	public List<Language> getLanguages(String username) {
		User user = (User) this.getCurrentSession().createQuery("from User where username="+"'"+username+"'").uniqueResult();
		List<Language> lans = (List<Language>) user.getLanguages();
		return lans;
	}

	@Override
	public void createUser(User user, UserRoles userroles) {
		user.setUserroles(userroles);
		this.getCurrentSession().save(user);
		this.getCurrentSession().save(userroles);
	}

}
