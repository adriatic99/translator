package com.translator.dao;

import java.util.List;

import com.translator.domain.Language;

public interface LanguageDAO<T> extends TranslatorDAO<T> {

	public List<Language> getLanguagesByUsername(String username);
}
