package com.translator.dao;

import java.util.List;
import java.util.Set;

import com.translator.domain.Language;
import com.translator.domain.Project;
import com.translator.domain.User;
import com.translator.domain.UserRoles;

public interface UserDAO<T> extends TranslatorDAO<T> {
	
	public User getUserByUsername(String username);
	public List<Project> getProjects(String username);
	public List<Language> getLanguages(String username);
	public void createUser(User user, UserRoles userroles);
}
