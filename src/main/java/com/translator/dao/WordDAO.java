package com.translator.dao;

import java.util.List;

import com.translator.domain.Language;
import com.translator.domain.Project;
import com.translator.domain.Word;

public interface WordDAO<T> extends TranslatorDAO<T> {

	public Word saveText(String name, String translation);
	public List<Word> getTextByName(String text);
}
