package com.translator.dao;

import java.util.Set;

import java.util.List;

import com.translator.domain.Project;
import com.translator.domain.Text;
import com.translator.domain.Type;

public interface TypeDAO<T> extends TranslatorDAO<T> {
	
	public Set<Text> getTexts(Type type);
	public Project getProject(Type type);
	public List<Type> getTypesByProjectID(int project);
}
