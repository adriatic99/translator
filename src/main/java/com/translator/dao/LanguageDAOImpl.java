package com.translator.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.translator.domain.Language;

public class LanguageDAOImpl<T> implements LanguageDAO<T> {

	@Autowired
	private SessionFactory session;
	
	public void setSession(SessionFactory session)
	{
		this.session = session;
	}
	
	private Session getCurrentSession()
	{
		return session.getCurrentSession();
	}
	
	@Override
	public T findById(int id) {
		Language lan = (Language) this.getCurrentSession().createQuery("from Language where languageID="+id).uniqueResult();
		return (T) lan;
	}

	@Override
	public List<T> findAll() {
		List<Language> lans = this.getCurrentSession().createQuery("from Language").list();
		return (List<T>) lans;
	}

	@Override
	public void update(T entity) {
		this.getCurrentSession().update(entity);
	}

	@Override
	public void save(T entity) {
		this.getCurrentSession().save(entity);
	}

	@Override
	public void delete(T entity) {
		this.getCurrentSession().delete(entity);
	}

	@Override
	public List<Language> getLanguagesByUsername(String username) {
		List<Language> lans = this.getCurrentSession().createQuery("from Language where user.username="+"\'"+username+"\'").list();
		return lans;
	}

}
