package com.translator.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.translator.domain.Language;
import com.translator.domain.Project;
import com.translator.domain.Word;

public class WordDAOImpl<T> implements WordDAO<T> {

	@Autowired
	private SessionFactory session;
	
	public void setSession(SessionFactory session)
	{
		this.session = session;
	}
	
	private Session getCurrentSession()
	{
		return session.getCurrentSession();
	}
	
	@Override
	public T findById(int id) {
		return (T) this.getCurrentSession().createQuery("from Word where wordID="+id).uniqueResult();
	}

	@Override
	public List<T> findAll() {
		return this.getCurrentSession().createQuery("from Word").list();
	}

	@Override
	public void update(T entity) {
		this.getCurrentSession().update(entity);
	}

	@Override
	public void save(T entity) {
		this.getCurrentSession().save(entity);
	}

	@Override
	public void delete(T entity) {
		this.getCurrentSession().delete(entity);
	}

	@Override
	public Word saveText(String name, String translation) {
		Word word = new Word(name,translation);
		this.getCurrentSession().save(word);
		return word;
	}

	@Override
	public List<Word> getTextByName(String text) {
		List<Word> words = this.getCurrentSession().createQuery("from Word where word="+"'"+text+"'").list();
		return words;
	}
}
