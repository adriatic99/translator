package com.translator.dao;

import java.util.List;

import com.translator.domain.Text;

public interface TextDAO<T> extends TranslatorDAO<T> {

	public List<Text> getTextList(int languageid, List<Integer> typesid, int projectid);
}
