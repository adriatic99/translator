package com.translator.dao;

import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.translator.domain.Language;
import com.translator.domain.Project;
import com.translator.domain.Type;
import com.translator.domain.User;

public class ProjectDAOImpl<T> implements ProjectDAO<T> {

	@Autowired
	private SessionFactory session;
	
	public void setSession(SessionFactory session)
	{
		this.session = session;
	}
	
	private Session getCurrentSession()
	{
		return session.getCurrentSession();
	}
	
	@Override
	public T findById(int id) {
		Project project = (Project) this.getCurrentSession().createQuery("from Project where projectID="+id).uniqueResult();
		return (T) project;
	}

	@Override
	public List<T> findAll() {
		List<Project> projects = this.getCurrentSession().createQuery("from Project").list();
		return (List<T>) projects;
	}

	@Override
	public void update(T entity) {
		this.getCurrentSession().update(entity);
	}

	@Override
	public void save(T entity) {
		this.getCurrentSession().save(entity);
	}

	@Override
	public void delete(T entity) {
		this.getCurrentSession().delete(entity);
	}

	@Override
	public List<Project> findByUsername(User user) {
		List<Project> projects = this.getCurrentSession().createQuery("from Project where username="+"'"+user.getUsername()+"'").list();
		return projects;
	}

	@Override
	public Set<Type> getTypes(int projectID) {
		Project project = (Project) this.getCurrentSession().createQuery("from Project where projectID="+projectID).uniqueResult();
		Set<Type> types = (Set<Type>) project.getTypes();
		return types;
	}

}
