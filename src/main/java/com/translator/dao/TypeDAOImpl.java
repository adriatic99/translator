package com.translator.dao;

import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.translator.domain.Language;
import com.translator.domain.Project;
import com.translator.domain.Text;
import com.translator.domain.Type;

public class TypeDAOImpl<T> implements TypeDAO<T> {

	@Autowired
	private SessionFactory session;
	
	public void setSession(SessionFactory session)
	{
		this.session = session;
	}
	
	private Session getCurrentSession()
	{
		return session.getCurrentSession();
	}
	
	@Override
	public T findById(int id) {
		Type type = (Type) this.getCurrentSession().createQuery("from Type where typeID="+id).uniqueResult();
		return (T) type;
	}

	@Override
	public List<T> findAll() {
		List<Type> types = this.getCurrentSession().createQuery("from Type").list();
		return (List<T>) types;
	}

	@Override
	public void update(T entity) {
		this.getCurrentSession().update(entity);
	}

	@Override
	public void save(T entity) {
		this.getCurrentSession().save(entity);	
	}

	@Override
	public void delete(T entity) {
		this.getCurrentSession().delete(entity);
	}

	@Override
	public Set<Text> getTexts(Type type) {
		int typeid = type.getTypeID();
		Type type1 = (Type) this.getCurrentSession().createQuery("from Type where typeID="+typeid).uniqueResult();
		Set<Text> texts = type1.getTexts();
		return texts;
	}

	@Override
	public Project getProject(Type type) {
		int typeid = type.getTypeID();
		Type type1 = (Type) this.getCurrentSession().createQuery("from Type where typeID="+typeid).uniqueResult();
		Project project = type1.getProject();
		return project;
	}

	@Override
	public List<Type> getTypesByProjectID(int project) {
		return this.getCurrentSession().createQuery("from Type where project.projectID="+project).list();
	}

}
