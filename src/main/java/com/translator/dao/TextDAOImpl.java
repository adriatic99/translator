package com.translator.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.translator.domain.Project;
import com.translator.domain.Text;
import com.translator.domain.Type;

public class TextDAOImpl<T> implements TypeDAO<T> {

	@Autowired
	private SessionFactory session;
	
	public void setSession(SessionFactory session)
	{
		this.session = session;
	}
	
	private Session getCurrentSession()
	{
		return session.getCurrentSession();
	}
	
	@Override
	public T findById(int id) {
		Text text = (Text) this.getCurrentSession().createQuery("from Text where textid="+id).uniqueResult();
		return (T) text;
	}

	@Override
	public List<T> findAll() {
		List<Text> texts = this.getCurrentSession().createQuery("from Text").list();
		return (List<T>) texts;
	}

	@Override
	public void update(T entity) {
		this.getCurrentSession().update(entity);
	}

	@Override
	public void save(T entity) {
		this.getCurrentSession().save(entity);
	}

	@Override
	public void delete(T entity) {
		this.getCurrentSession().delete(entity);
	}

	@Override
	public Set<Text> getTexts(Type type) {
		return null;
	}

	@Override
	public Project getProject(Type type) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Type> getTypesByProjectID(int project) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public List<Text> getTextList(int languageid, List<Integer> typesid, int projectid)
	{
		if(typesid == null)
			return null;
		String query;
		query = "from Text where language.languageid="+languageid+"and project.projectID="+projectid;
		List<Text> textsTypes = this.getCurrentSession().createQuery(query).list();
		List<Text> texts = new ArrayList<Text>();
		for(Text text : textsTypes)
		{
			boolean result = false;
			for(Type type : text.getTypes())
			{
				for(Integer id : typesid)
				{
					if(id.intValue()==type.getTypeID())
					{
						result = true;
						break;
					}
				}
				if(result)
				{
					texts.add(text);
					break;
				}
			}
		}
		return texts;
	}

}
