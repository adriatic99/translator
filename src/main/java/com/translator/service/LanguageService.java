package com.translator.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.translator.dao.LanguageDAO;
import com.translator.dao.ProjectDAO;
import com.translator.dao.UserDAO;
import com.translator.domain.Language;
import com.translator.domain.Project;
import com.translator.domain.User;

@Service
public class LanguageService {

	private LanguageDAO languageDAO;
	
	@Autowired
	public void setLanguage(LanguageDAO languageDAO)
	{
		this.languageDAO = languageDAO;
	}
	
	@Transactional
	public void createLanguage(String name, String code, User user)
	{
		Language project = new Language(name, code, user);
		this.languageDAO.save(project);
	}
	
	@Transactional
	public void updateLanguage(Language language)
	{
		this.languageDAO.update(language);
	}
	
	@Transactional
	public void deleteLanguage(Language language)
	{	
		this.languageDAO.delete(language);;
	}
	
	@Transactional
	public List<Language> getLanguages()
	{
		return this.languageDAO.findAll();
	}
	
	@Transactional
	public Language getLanguageById(int lanid)
	{
		return (Language) this.languageDAO.findById(lanid);
	}
	
	@Transactional
	public List<Language> getLanguagesByUsername(String username)
	{
		return this.languageDAO.getLanguagesByUsername(username);
	}
}
