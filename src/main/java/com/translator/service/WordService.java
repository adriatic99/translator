package com.translator.service;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.translator.dao.ProjectDAO;
import com.translator.dao.UserDAO;
import com.translator.dao.WordDAO;
import com.translator.domain.Language;
import com.translator.domain.Project;
import com.translator.domain.User;
import com.translator.domain.Word;
import com.translator.domain.Text;

@Service
public class WordService {

	private WordDAO wordDAO;
	
	@Autowired
	public void setWord(WordDAO wordDAO)
	{
		this.wordDAO = wordDAO;
	}
	
	@Transactional
	public Word saveWord(String word, String translation)
	{
		Word w = this.wordDAO.saveText(word, translation);
		return w;
	}
	
	@Transactional
	public void createWord(String word, String translation, Language lan, Set<Text> texts)
	{
		Word w = new Word(word, translation, lan, texts);
		this.wordDAO.save(w);
	}
	
	@Transactional
	public void createWord(String word, String translation)
	{
		Word w = new Word(word, translation);
		this.wordDAO.save(w);
	}
	
	@Transactional
	public void updateWord(Word word)
	{
		this.wordDAO.update(word);
	}
	
	@Transactional
	public void deleteWord(int wordID)
	{	
		Word word = (Word) this.wordDAO.findById(wordID);
		this.wordDAO.delete(word);
	}
	
	@Transactional
	public List<Word> getWords()
	{
		return this.wordDAO.findAll();
	}
	
	@Transactional
	public Word getWordById(int wordid)
	{
		return (Word) this.wordDAO.findById(wordid);
	}
	
	@Transactional
	public List<Word> getWordsByName(String text)
	{
		return this.wordDAO.getTextByName(text);
	}
}
