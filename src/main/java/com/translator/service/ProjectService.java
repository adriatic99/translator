package com.translator.service;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.translator.dao.ProjectDAO;
import com.translator.dao.UserDAO;
import com.translator.domain.Project;
import com.translator.domain.Type;
import com.translator.domain.User;

@Service
public class ProjectService {

	private ProjectDAO projectDAO;
	
	@Autowired
	public void setProject(ProjectDAO projectDAO)
	{
		this.projectDAO = projectDAO;
	}
	
	@Transactional
	public void createProject(String name, User user)
	{
		Project project = new Project(name,user);
		this.projectDAO.save(project);
	}
	
	@Transactional
	public void updateProject(Project project)
	{
		this.projectDAO.update(project);
	}
	
	@Transactional
	public void deleteProject(int projectID)
	{	
		Project project = (Project) this.projectDAO.findById(projectID);
		this.projectDAO.delete(project);;
	}
	
	@Transactional(readOnly=true)
	public List<Project> getProjects()
	{
		return this.projectDAO.findAll();
	}
	
	@Transactional(readOnly=true)
	public Project getProjectById(int projectid)
	{
		return (Project) this.projectDAO.findById(projectid);
	}
	
	@Transactional(readOnly=true)
	public Set<Type> getTypes(int projectid)
	{
		return this.projectDAO.getTypes(projectid);
	}
}
