package com.translator.service;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.translator.dao.UserDAO;
import com.translator.domain.Language;
import com.translator.domain.User;
import com.translator.domain.UserRoles;

@Service
public class UserService {

	private UserDAO userDAO;
	
	@Autowired
	public void setUser(UserDAO userDAO)
	{
		this.userDAO = userDAO;
	}
	
	@Transactional
	public Set<Language> getLanguages(User user)
	{
		Set<Language> lans = user.getLanguages();
		return lans;
	}
	
	@Transactional
	public void createUser(String username, String password)
	{
		User user = new User(username,password);
		this.userDAO.save(user);
	}
	
	@Transactional
	public void createUser(String username, String password, String userroles)
	{
		User user = new User(username,password);
		UserRoles uroles = new UserRoles();
		uroles.setUser(user);
		user.setUserroles(uroles);
		this.userDAO.createUser(user,uroles);
	}
	
	@Transactional
	public void updateUser(String username, String password)
	{
		User user = this.userDAO.getUserByUsername(username);
		user.setPassword(password);
		this.userDAO.update(user);
	}
	
	@Transactional
	public void updatedeleteUser(User user)
	{
		this.userDAO.update(user);
	}
	
	@Transactional
	public void deleteUser(String username)
	{	
		User user = this.userDAO.getUserByUsername(username);
		user.setEnabled(false);
		this.updatedeleteUser(user);
		//this.userDAO.delete(user);
	}
	
	@Transactional
	public List<User> getUsers()
	{
		return this.userDAO.findAll();
	}
	
	@Transactional
	public User getUserByUsername(String username)
	{
		return this.userDAO.getUserByUsername(username);
	}
	
}
