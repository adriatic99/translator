package com.translator.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.translator.dao.ProjectDAO;
import com.translator.dao.TypeDAO;
import com.translator.dao.UserDAO;
import com.translator.domain.Project;
import com.translator.domain.Type;
import com.translator.domain.User;

@Service
public class TypeService {

	private TypeDAO typeDAO;
	
	@Autowired
	public void setType(TypeDAO typeDAO)
	{
		this.typeDAO = typeDAO;
	}
	
	@Transactional
	public void createType(String name, Project project)
	{
		Type type = new Type(name,project);
		this.typeDAO.save(type);
	}
	
	@Transactional
	public void updateType(Type type)
	{
		this.typeDAO.update(type);
	}
	
	@Transactional
	public void deleteType(int typeID)
	{	
		Type type = (Type) this.typeDAO.findById(typeID);
		this.typeDAO.delete(type);
	}
	
	@Transactional
	public List<Type> getTypes()
	{
		return this.typeDAO.findAll();
	}
	
	@Transactional
	public Type getTypeById(int typeid)
	{
		return (Type) this.typeDAO.findById(typeid);
	}
	
	@Transactional
	public List<Type> getTypesByProjectID(int project) {
		return this.typeDAO.getTypesByProjectID(project);
	}
}
