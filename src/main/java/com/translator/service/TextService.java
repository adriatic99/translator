package com.translator.service;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.translator.dao.ProjectDAO;
import com.translator.dao.TextDAO;
import com.translator.dao.TextDAOImpl;
import com.translator.dao.UserDAO;
import com.translator.domain.Language;
import com.translator.domain.Project;
import com.translator.domain.Text;
import com.translator.domain.User;
import com.translator.domain.Word;
import com.translator.domain.Type;

@Service
public class TextService {

	private TextDAOImpl textDAO;
	
	@Autowired
	public void setTextDAO(TextDAOImpl textDAO)
	{
		this.textDAO = textDAO;
	}
	
	@Transactional
	public void createText(Project project, Language lan, String title, String text, String link, Set<Word> words, Set<Type> types)
	{
		Text t = new Text(project, lan, title, text, link, words, types);
		this.textDAO.save(t);
	}
	
	@Transactional
	public void updateText(Text text)
	{
		this.textDAO.update(text);
	}
	
	@Transactional
	public void deleteText(int textID)
	{	
		Text text = (Text) this.textDAO.findById(textID);
		this.textDAO.delete(text);
	}
	
	@Transactional
	public List<Text> getTexts()
	{
		return this.textDAO.findAll();
	}
	
	@Transactional
	public Text getTextById(int textID)
	{
		return (Text) this.textDAO.findById(textID);
	}
	
	@Transactional
	public List<Text> getSearchText(int lanid, List<Integer> typesid, int projectid)
	{
		return this.textDAO.getTextList(lanid, typesid, projectid);
	}
}
