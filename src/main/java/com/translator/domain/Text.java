package com.translator.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="texts")
public class Text {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "textID", unique = true, nullable = false)
	private int textID;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "projectID", nullable = false)
	@JsonBackReference
	private Project project;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "languageID", nullable = false)
	@JsonBackReference
	private Language language;
	@Column(name="title")
	private String title;
	@Column(name = "text")
	private String text;
	@Column(name = "link")
	private String link;
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "texts_words", catalog = "translator2", joinColumns = { 
			@JoinColumn(name = "textID", nullable = false, updatable = false) }, 
			inverseJoinColumns = { @JoinColumn(name = "wordID", 
					nullable = false, updatable = false) })
	@JsonBackReference
	public Set<Word> words;
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "texts_types", catalog = "translator2", joinColumns = { 
			@JoinColumn(name = "textID", nullable = false, updatable = false) }, 
			inverseJoinColumns = { @JoinColumn(name = "typeID", 
					nullable = false, updatable = false) })
	@JsonBackReference
	public Set<Type> types;
	
	public Text() {}
	
	public Text(Project project, Language lan, String title, String text, String link, Set<Word> words, Set<Type> types)
	{
		this.project = project;
		this.language = lan;
		this.text = text;
		this.link = link;
		this.words = words;
		this.types = types;
		this.title = title;
	}
	
	public int getTextID() {
		return textID;
	}
	public void setTextID(int textID) {
		this.textID = textID;
	}
	public Project getProject() {
		return project;
	}
	public void setProject(Project project) {
		this.project = project;
	}
	public Language getLanguage() {
		return language;
	}
	public void setLanguage(Language language) {
		this.language = language;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public Set<Word> getWords() {
		return words;
	}
	public void setWords(Set<Word> words) {
		this.words = words;
	}
	public Set<Type> getTypes() {
		return types;
	}
	public void setTypes(Set<Type> types) {
		this.types = types;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
