package com.translator.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="types")
public class Type {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "typeID", unique = true, nullable = false)
	private int typeID;
	@Column(name = "name")
	private String name;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "projectID", nullable = false)
	@JsonBackReference
	private Project project;
	@ManyToMany(fetch = FetchType.EAGER, mappedBy = "types")
	@JsonManagedReference
	private Set<Text> texts = new HashSet<Text>(0);
	
	public Type(String name, Project project) {
		this.name = name;
		this.project = project;
	}
	
	public Type() {}

	public int getTypeID() {
		return this.typeID;
	}

	public void setTypeID(int typeID) {
		this.typeID = typeID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}
	
	public Set<Text> getTexts() {
		return texts;
	}

	public void setTexts(Set<Text> texts) {
		this.texts = texts;
	}
}
