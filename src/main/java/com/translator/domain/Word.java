package com.translator.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="words")
public class Word {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "wordID", unique = true, nullable = false)
	private int wordID;
	@Column(name = "word")
	private String word;
	@Column(name = "translation")
	private String translation;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "languageID", nullable = false)
	@JsonBackReference
	private Language language;
	@ManyToMany(fetch = FetchType.EAGER, mappedBy = "words")
	@JsonManagedReference
	private Set<Text> texts = new HashSet<Text>(0);
	
	public Word() {}
	
	public Word(String word, String translation, Language lan, Set<Text> texts)
	{
		this.word = word;
		this.translation = translation;
		this.language = lan;
		this.texts = texts;
	}
	
	public Word(String word, String translation)
	{
		this.word = word;
		this.translation = translation;
	}

	public int getWordID() {
		return wordID;
	}

	public void setWordID(int wordID) {
		this.wordID = wordID;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public String getTranslation() {
		return translation;
	}

	public void setTranslation(String translation) {
		this.translation = translation;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public Set<Text> getTexts() {
		return texts;
	}

	public void setTexts(Set<Text> texts) {
		this.texts = texts;
	}
	
	
}
