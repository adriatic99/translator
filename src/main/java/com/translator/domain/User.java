package com.translator.domain;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.OneToMany;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.util.Set;

@Entity
@Table(name = "users")
public class User {
	
	@Id
	@Column(name = "username")
	@Size(min=2,max=9,message = "Your username must between 2 and 9 characters")
	private String username;
	@Column(name = "password")
	@NotNull
	@Size(min=2,max=7,message = "Your password must between 2 and 7 characters")
	private String password;
	@Column(name = "enabled", columnDefinition = "TINYINT")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean enabled;
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
	@JsonManagedReference
	private Set<Project> projects;
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
	@JsonManagedReference
	private Set<Language> languages;
	@Transient
	private String confirmPassword;
	@OneToOne(fetch = FetchType.EAGER,mappedBy = "user")
	@JsonManagedReference
	private UserRoles userroles;
	
	public User()
	{
		this.enabled = true;
	}
	
	public User(String username, String password)
	{
		this.username = username;
		this.password = password;
		this.enabled = true;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	public Set<Project> getProjects() {
		return this.projects;
	}

	public void setProjects(Set<Project> projects) {
		this.projects = projects;
	}
	
	public Set<Language> getLanguages() {
		return this.languages;
	}

	public void setLanguages(Set<Language> languages) {
		this.languages = languages;
	}
	
	public String getConfirmPassword()
	{
		return this.confirmPassword;
	}
	
	public void setConfirmPassword(String pwd)
	{
		this.confirmPassword = pwd;
	}
	
	public UserRoles getUserroles()
	{
		return this.userroles;
	}
	
	public void setUserroles(UserRoles userrole)
	{
		this.userroles = userrole;
	}
}
