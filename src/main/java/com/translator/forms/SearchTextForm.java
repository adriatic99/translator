package com.translator.forms;

import java.util.List;

import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Range;

public class SearchTextForm {
	
	private int langid;
	private List<Integer> typesid;
	private int projectid;
	
	public SearchTextForm() {}
	
	public SearchTextForm(int langid, List<Integer> typesid, int projectid) {
		super();
		this.langid = langid;
		this.typesid = typesid;
		this.projectid = projectid;
	}
	
	public int getLangid() {
		return langid;
	}
	public void setLangid(int langid) {
		this.langid = langid;
	}
	public List<Integer> getTypesid() {
		return typesid;
	}
	public void setTypesid(List<Integer> typesid) {
		this.typesid = typesid;
	}
	public int getProjectid() {
		return projectid;
	}
	public void setProjectid(int projectid) {
		this.projectid = projectid;
	}
	
	public void addType(Integer type)
	{
		this.typesid.add(type);
	}

}
