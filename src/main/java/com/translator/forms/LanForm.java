package com.translator.forms;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class LanForm {

	private String name;
	private String code;
	private int projectid;
	
	public LanForm() {}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LanForm(String name, String code, int projectid) {
		super();
		this.name = name;
		this.code = code;
		this.projectid = projectid;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getProjectid() {
		return projectid;
	}

	public void setProjectid(int projectid) {
		this.projectid = projectid;
	}
	
	
}
