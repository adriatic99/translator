package com.translator.forms;

public class TypeForm {

	private int typeid;
	private String name;
	private int projectid;
	
	public TypeForm() {}
	
	public TypeForm(int typeid, String name, int projectid) {
		super();
		this.typeid = typeid;
		this.name = name;
		this.projectid = projectid;
	}

	public int getTypeid() {
		return typeid;
	}

	public void setTypeid(int typeid) {
		this.typeid = typeid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getProjectid() {
		return projectid;
	}

	public void setProjectid(int projectid) {
		this.projectid = projectid;
	}
	
	
}
