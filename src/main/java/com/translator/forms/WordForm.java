package com.translator.forms;

public class WordForm {

	private String name;
	private String translation;
	
	public WordForm() {}
	
	public WordForm(String name, String translation)
	{
		this.name = name;
		this.translation = translation;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTranslation() {
		return translation;
	}
	public void setTranslation(String translation) {
		this.translation = translation;
	}
}
