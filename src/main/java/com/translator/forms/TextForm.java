package com.translator.forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.translator.domain.Word;

public class TextForm {
	
	private String title;
	private String text;
	private String link;
	private int languageid;
	private int projectid;
	private List<Integer> typeids;
	private List<Integer> words = new ArrayList<Integer>();

	public TextForm() {
	}

	public TextForm(String title, String text, String link, int languageid, int projectid, List<Integer> typeids, List<Integer> maps) {
		this.title = title;
		this.text = text;
		this.link = link;
		this.languageid = languageid;
		this.projectid = projectid;
		this.typeids = typeids;
		this.words = maps;
	}
	
	public List<Integer> getTypeids() {
		return typeids;
	}

	public void setTypeids(List<Integer> typeids) {
		this.typeids = typeids;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public int getLanguageid() {
		return languageid;
	}

	public void setLanguageid(int languageid) {
		this.languageid = languageid;
	}

	public int getProjectid() {
		return projectid;
	}

	public void setProjectid(int projectid) {
		this.projectid = projectid;
	}

	public List<Integer> getWords() {
		return words;
	}

	public void setWords(List<Integer> words) {
		this.words = words;
	}
	
	public void setWord(Integer value)
	{
		this.words.add(value);
	}
	
	public void deleteWord(Integer word)
	{
		this.words.remove(word);
	}
}
