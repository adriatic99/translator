package com.translator.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.translator.domain.User;
import com.translator.domain.Word;
import com.translator.forms.WordForm;
import com.translator.service.LanguageService;
import com.translator.service.ProjectService;
import com.translator.service.WordService;

@Controller
@RequestMapping(value = "/word")
public class WordController {

	private WordService wordService;
	private LanguageService languageService;
	private ProjectService projectService;
	
	private static final Logger logger = LoggerFactory.getLogger(WordController.class);
    
    @Autowired(required=true)
    @Qualifier(value="wordService")
    public void setWordService(WordService ws){
        this.wordService = ws;
        logger.info("word service set");
    }
    
    @Autowired(required=true)
    @Qualifier(value="languageService")
    public void setLanguageService(LanguageService ls){
        this.languageService = ls;
        logger.info("language service set");
    }
    
    @Autowired(required=true)
    @Qualifier(value="projectService")
    public void setProjectService(ProjectService ps){
        this.projectService = ps;
        logger.info("project service set");
    }
    
    @RequestMapping(value="/create",method = RequestMethod.POST, consumes = {"application/json"}, produces={"application/json"})
    public @ResponseBody Word createWord(@RequestBody WordForm body, Model model)
    {	
    	Word word = this.wordService.saveWord(body.getName(), body.getTranslation());
		return word;	
    }
    
    @RequestMapping(value="/find",method = RequestMethod.GET, headers="Accept=application/json")
    public @ResponseBody List<Word> findWord(@RequestParam String text, Model model)
    {		
    	List<Word> words = this.wordService.getWordsByName(text);
    	return words;
    }
}
