package com.translator.controller;

import java.security.Principal;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import com.translator.service.UserService;
import com.translator.domain.Project;
import com.translator.domain.User;
import com.translator.service.UserService;

@Controller
public class UserController {
	
	private UserService userService;
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
    
    @Autowired(required=true)
    @Qualifier(value="userService")
    public void setUserService(UserService sp){
        this.userService = sp;
        logger.info("user service set");
    }
	
	/*
	@RequestMapping(value = "/user/create", method = RequestMethod.POST)
	public String createuser(@ModelAttribute("User")User user, Locale locale, Model model) {
		logger.info("Create User by: ", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("username", formattedDate);
		model.addAttribute("password", formattedDate);
		
		
		return "home";
	}
	*/
    /*
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(
		@RequestParam(value = "error", required = false) String error,
		@RequestParam(value = "logout", required = false) String logout) {
 
		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", "Invalid username and password!");
		}
 
		if (logout != null) {
			model.addObject("msg", "You've been logged out successfully.");
		}
		model.setViewName("login");
 
		return model;
 
	}
	*/
	
    @RequestMapping(value = "/user/create", method = RequestMethod.POST)
	public String createUser(@Valid @ModelAttribute("userForm") User user, BindingResult bindingResult, Model model) {
		logger.info("Create User by: ", user.getUsername());
		
		String username = user.getUsername();
		String password = user.getPassword();
		String confirm = user.getConfirmPassword();
		model.addAttribute("username", username);
		model.addAttribute("password", password);
		
		if (bindingResult.hasErrors()) {
            return "registration";
        }
		
		User userExt = this.userService.getUserByUsername(username);
		if(userExt != null)
		{
			model.addAttribute("usernameError","the username already exists");
			return "registration";
		}
		
		if(confirm.equals(password))
		{}
		else
		{
			model.addAttribute("error","Confirm Password and Password don't match!");
			return "registration";
		}
		
		this.userService.createUser(username, password, "ROLE_USER");
		
		return "home";
	}
    
	@RequestMapping(value = "/user/create", method = RequestMethod.GET)
	public String createUserGet(@Valid @ModelAttribute("userForm") User user, Model model, BindingResult bindingResult) {
		logger.info("Create User by: ", user.getUsername());
		
		if (bindingResult.hasErrors()) {
            return "user";
        }
		
		model.addAttribute("username", user.getUsername());
		model.addAttribute("password", user.getPassword());
		
		//this.userService.createUser(username, password);
		
		return "home";
	}
	
	@RequestMapping(value = "/user/update/{username}/{password}", method = RequestMethod.GET)
	public String updateUser(@PathVariable String username, @PathVariable String password, Model model) {
		
		this.userService.updateUser(username, password);
		
		return "home";
	}
	
	@RequestMapping(value = "/user/update", method = RequestMethod.GET)
	public String updateUser1(@RequestParam("username") String username, @RequestParam("password") String password, Model model) {
		
		this.userService.updateUser(username, password);
		
		return "home";
	}
	
	@RequestMapping(value = "/user/delete", method = RequestMethod.GET)
	public String deleteUser(@RequestParam("username") String username, Model model) {
		
		this.userService.deleteUser(username);
		
		return "home";
	}
	
	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public String users(Model model) {
		
		List<User> users = this.userService.getUsers();
		
		return "home";
	}
	
	@RequestMapping(value = "/user/show", method = RequestMethod.GET)
	public String showUser(@RequestParam("username") String username, Model model) {
		
		User user = this.userService.getUserByUsername(username);
		
		return "home";
	}
	
	/*
	@RequestMapping(value="/user/projects", method = RequestMethod.GET)
	public @ResponseBody Set<Project> getProjects(Model model, Principal principe)
	{
		String uname = principe.getName();
		User user = this.userService.getUserByUsername(uname);
		Set<Project> projects = user.getProjects();
	    
	    return projects;
	}
	*/
	
	@RequestMapping(value="/user/project/1", method = RequestMethod.GET)
	public @ResponseBody int getProjects1(@RequestParam String username, Model model)
	{
		User user = this.userService.getUserByUsername(username);
		Set<Project> projects = user.getProjects();
	    
	    return projects.size();
	}
	
	@RequestMapping(value="/user/registration", method = RequestMethod.GET)
	public String userRegistration(Model model)
	{
		User user = new User();
		model.addAttribute("userForm", user);
		return "registration";
	}
	
	/*
	@RequestMapping(value="/user/login", method = RequestMethod.GET)
	public String userLogin(Model model)
	{
		User user = new User();
		model.addAttribute("userForm", user);
		return "login";
	}
	
	
	@RequestMapping(value = "/user/login", method = RequestMethod.POST)
	public String loginUser(@ModelAttribute("userForm") User user, Model model) {
		logger.info("Create User by: ", user.getUsername());
		
		String username = user.getUsername();
		String password = user.getPassword();
		
		String error = "Wrong data, try it again!";
		
		User userExt = this.userService.getUserByUsername(username);
		if(userExt == null)
		{
			model.addAttribute("error", error);
			return "login";
		}
		
		if(userExt.getPassword().equals(password)) {}
		else
		{
			model.addAttribute("error", error);
			return "login";
		}
		
		if(!userExt.isEnabled())
		{
			error = "the user is not active anymore";
			model.addAttribute("error", error);
			return "login";
		}
		
		model.addAttribute("username", user.getUsername());
		model.addAttribute("password", user.getPassword());
		
		//this.userService.createUser(username, password);
		
		return "home";
	}
	*/
}
