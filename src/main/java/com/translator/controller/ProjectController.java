package com.translator.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.translator.domain.Language;
import com.translator.domain.Project;
import com.translator.domain.Type;
import com.translator.domain.User;
import com.translator.domain.Word;
import com.translator.forms.LanForm;
import com.translator.forms.TextForm;
import com.translator.forms.TypeForm;
import com.translator.service.ProjectService;
import com.translator.service.UserService;

@Controller
public class ProjectController {

	private ProjectService projectService;
	private static final Logger logger = LoggerFactory.getLogger(ProjectController.class);
    
    @Autowired(required=true)
    @Qualifier(value="projectService")
    public void setProjectService(ProjectService ps){
        this.projectService = ps;
        logger.info("project service set");
    }
    
    
    private UserService userService;
    
    @Autowired(required=true)
    @Qualifier(value="userService")
    public void setUserService(UserService sp){
        this.userService = sp;
        logger.info("user service set");
    }
    
    @RequestMapping(value = "/insert", method = RequestMethod.GET)
	public String insert(@RequestParam int projectid, Model model, Principal principal) {
    	
    	int id = projectid;
    	String name = principal.getName();
    	User user = this.userService.getUserByUsername(name);
    	Project project = this.projectService.getProjectById(projectid);
    	model.addAttribute("project", project);
    	model.addAttribute("username", name);
    	LanForm lan = new LanForm();
    	model.addAttribute("lanForm", lan);
    	Map<Integer,String> lansMap = new LinkedHashMap<Integer,String>();
    	Set<Language> lans = this.userService.getLanguages(user);
    	Iterator iter = lans.iterator();
    	while(iter.hasNext())
    	{
    		Language l = (Language) iter.next();
    		lansMap.put(l.getLanguageid(), l.getName());
    	}
    	model.addAttribute("lans",lansMap);
    	
    	//TypeForm
    	TypeForm type = new TypeForm();
    	model.addAttribute("typeForm",type);
    	
    	Map<Integer,String> typesMap = new LinkedHashMap<Integer,String>();
    	Set<Type> types = this.projectService.getTypes(projectid);
    	Iterator iterType = types.iterator();
    	while(iterType.hasNext())
    	{
    		Type t = (Type) iterType.next();
    		typesMap.put(t.getTypeID(), t.getName());
    	}
    	model.addAttribute("type",typesMap);
    	
    	//TextForm
    	List<Word> lwords = new ArrayList<Word>();
    	TextForm textForm = new TextForm();
    	textForm.setTitle("titleForm");
    	textForm.setText("textForm");
    	textForm.setLink("linkForm");
    	List<Word> words = new ArrayList<Word>();
    	Word w = new Word();
    	w.setWord("word1");
    	w.setTranslation("trans1");
    	w.setWordID(10);
    	words.add(w);
    	//textForm.setWord(w);
    	lwords.add(w);
    	model.addAttribute("cbWords",lwords);
    	model.addAttribute("textForm",textForm);
    	
    	//WordForm
    	Word word = new Word();
    	model.addAttribute("wordForm",word);
    	
		return "insert";
	}
    
    @RequestMapping(value = "/project/create", method = RequestMethod.GET)
	public String createUser(@RequestParam String name, @RequestParam String username, Model model) {
		logger.info("Create User by: ", username);
		
		Project p = new Project();
		model.addAttribute("userForm", p);
		
		User user = this.userService.getUserByUsername(username);
		if(name.length() < 3)
			model.addAttribute("error", "the name of the project has to have at least 3 characters");
		else
			this.projectService.createProject(name, user);
		
		return "projects";
	}
    
    @RequestMapping(value = "/projects", method = RequestMethod.GET)
    public String getProjects(Model model, Principal user)
    {
    	Set<Project> pro = this.userService.getUserByUsername(user.getName()).getProjects();
    	Project p = new Project();
		model.addAttribute("userForm", p);
    	model.addAttribute("projects", pro);
    	return "projects";
    }
    
    @RequestMapping(value = "/project/create", method = RequestMethod.POST)
	public String createProject(@Valid @ModelAttribute("userForm") Project proj, BindingResult bindingResult, Model model, Principal principal) {
		
		String username = principal.getName();
		
		if (bindingResult.hasErrors()) {
            return "projects";
        }
		
		User user = this.userService.getUserByUsername(username);
		this.projectService.createProject(proj.getName(), user);
		return "redirect:/projects";
	}
	
}
