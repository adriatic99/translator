package com.translator.controller;

import java.security.Principal;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.translator.domain.Language;
import com.translator.domain.Project;
import com.translator.domain.User;
import com.translator.forms.LanForm;
import com.translator.service.LanguageService;
import com.translator.service.UserService;
import com.translator.validator.LanFormValidator;

@Controller
@RequestMapping(value = "/language")
public class LanguageController {

    private LanFormValidator lanformValidator;
	private LanguageService languageService;
	private UserService userService;
	private static final Logger logger = LoggerFactory.getLogger(LanguageController.class);
    
	@Autowired(required=true)
    @Qualifier(value="lanformValidator")
    public void setLanformValidator(LanFormValidator lfv){
        this.lanformValidator = lfv;
        logger.info("lanform validator set");
    }
	
    @Autowired(required=true)
    @Qualifier(value="languageService")
    public void setLanguageService(LanguageService ls){
        this.languageService = ls;
        logger.info("language service set");
    }
    
    @Autowired(required=true)
    @Qualifier(value="userService")
    public void setUserService(UserService us){
        this.userService = us;
        logger.info("user service set");
    }
    
    @RequestMapping(value = "/create", method = RequestMethod.POST)
	public String createLanguage(@ModelAttribute("lanform") LanForm lanform, Model model, BindingResult result, Principal user1) {
		
		lanformValidator.validate(lanform, result);
		if(result.hasErrors())
		{
			logger.info("result has errors");
			logger.info(result.getFieldError().getCode());
			logger.info(result.getFieldError().getDefaultMessage());
			logger.info(result.getFieldError().getField());
			logger.info(result.getFieldError().getRejectedValue().toString());
	    	List<Language> lans = this.languageService.getLanguagesByUsername(user1.getName());
	    	model.addAttribute("list",lans);
	    	model.addAttribute("lanform",lanform);
	    	model.addAttribute("projectid",lanform.getProjectid());
	    	model.addAttribute("code",result.getFieldError().getCode());
	    	model.addAttribute("defaultmessage",result.getFieldError().getDefaultMessage());
	    	model.addAttribute("field",result.getFieldError().getField());
	    	model.addAttribute("reject",result.getFieldError().getRejectedValue().toString());
	    	model.addAttribute("projectid",lanform.getProjectid());
			return "languages";
		}
		
		User user = this.userService.getUserByUsername(user1.getName());
		this.languageService.createLanguage(lanform.getName(), lanform.getCode(), user);
		
		logger.info("New Language has been created");
		
		return "redirect:/insert?projectid="+lanform.getProjectid();
	}
    
    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public String newLanguage(@RequestParam int projectid, Model model, Principal user)
    {
    	LanForm lf = new LanForm();
    	List<Language> lans = this.languageService.getLanguagesByUsername(user.getName());
    	model.addAttribute("list",lans);
    	model.addAttribute("lanform",lf);
    	model.addAttribute("projectid",projectid);
    	return "languages";
    }
}
