package com.translator.controller;

import java.security.Principal;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.translator.domain.Project;
import com.translator.domain.Type;
import com.translator.domain.User;
import com.translator.forms.LanForm;
import com.translator.forms.TypeForm;
import com.translator.service.LanguageService;
import com.translator.service.ProjectService;
import com.translator.service.TypeService;
import com.translator.validator.LanFormValidator;
import com.translator.validator.TypeFormValidator;

@Controller
@RequestMapping(value = "/type")
public class TypeController {

	private TypeFormValidator typeformValidator;
	private TypeService typeService;
	private ProjectService projectService;
	private static final Logger logger = LoggerFactory.getLogger(TypeController.class);
    
	@Autowired(required=true)
    @Qualifier(value="typeformValidator")
    public void setTypeformValidator(TypeFormValidator tfv){
        this.typeformValidator = tfv;
        logger.info("type form validator set");
    }
	
    @Autowired(required=true)
    @Qualifier(value="typeService")
    public void setTypeService(TypeService ts){
        this.typeService = ts;
        logger.info("type service set");
    }
    
    @Autowired(required=true)
    @Qualifier(value="projectService")
    public void setProjectService(ProjectService ps){
        this.projectService = ps;
        logger.info("project service set");
    }
    
    @RequestMapping(value = "/create", method = RequestMethod.POST)
	public String createProject(@ModelAttribute("typeform") TypeForm typeForm, Model model, BindingResult result, Principal principal) {
		
    	this.typeformValidator.validate(typeForm, result);
    	if(result.hasErrors())
    	{
    		model.addAttribute("typeform", typeForm);
    		model.addAttribute("projectid", typeForm.getProjectid());
    		return "types";
    	}
		String username = principal.getName();
		Project project = this.projectService.getProjectById(typeForm.getProjectid());
		this.typeService.createType(typeForm.getName(), project);
		
		return "redirect:/insert?projectid="+typeForm.getProjectid();
	}
    
    @RequestMapping(value = "/new", method = RequestMethod.GET)
	public String createProject(@RequestParam int projectid, Model model, Principal principal) {
		
    	List<Type> list = this.typeService.getTypesByProjectID(projectid);
    	TypeForm tf = new TypeForm();
    	model.addAttribute("typeform", tf);
    	model.addAttribute("projectid", projectid);
    	model.addAttribute("list", list);
    	return "types";
	}
}
