package com.translator.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.translator.domain.Language;
import com.translator.domain.Project;
import com.translator.domain.Text;
import com.translator.domain.Type;
import com.translator.domain.User;
import com.translator.domain.Word;
import com.translator.forms.SearchTextForm;
import com.translator.forms.TextForm;
import com.translator.service.LanguageService;
import com.translator.service.ProjectService;
import com.translator.service.TextService;
import com.translator.service.TypeService;
import com.translator.service.UserService;
import com.translator.service.WordService;
import com.translator.validator.TextFormValidator;

@Controller
@RequestMapping(value = "/text")
public class TextController {

	private TextFormValidator textformValidator;
	private TextService textService;
	private WordService wordService;
	private LanguageService languageService;
	private ProjectService projectService;
	private TypeService typeService;
	private UserService userService;
	private static final Logger logger = LoggerFactory.getLogger(TextController.class);
    
    @Autowired(required=true)
    @Qualifier(value="textService")
    public void setTextService(TextService ts){
        this.textService = ts;
        logger.info("text service set");
    }
    
    @Autowired(required=true)
    @Qualifier(value="wordService")
    public void setWordService(WordService ws){
        this.wordService = ws;
        logger.info("word service set");
    }
    
    @Autowired(required=true)
    @Qualifier(value="languageService")
    public void setLanguageService(LanguageService ls){
        this.languageService = ls;
        logger.info("language service set");
    }
    
    @Autowired(required=true)
    @Qualifier(value="projectService")
    public void setProjectService(ProjectService ps){
        this.projectService = ps;
        logger.info("project service set");
    }
    
    @Autowired(required=true)
    @Qualifier(value="typeService")
    public void setTypeService(TypeService ts){
        this.typeService = ts;
        logger.info("type service set");
    }
    
    @Autowired(required=true)
    @Qualifier(value="userService")
    public void setUserService(UserService us){
        this.userService = us;
        logger.info("user service set");
    }
    
    @Autowired(required=true)
    @Qualifier(value="textformValidator")
    public void setTextFormValidator(TextFormValidator tfv)
    {
    	this.textformValidator = tfv;
    }
    
    @InitBinder("textForm")
    private void initBinder(WebDataBinder binder)
    {
    	binder.setValidator(this.textformValidator);
    }
    
    @RequestMapping(value="/search", method=RequestMethod.POST)
    public String displayTextList(@ModelAttribute("searchform") SearchTextForm stForm, Model model, Principal user, final RedirectAttributes redirectAttributes)
    {
    	int projectid = stForm.getProjectid();
    	List<Text> texts = this.textService.getSearchText(stForm.getLangid(), stForm.getTypesid(), stForm.getProjectid());
    	redirectAttributes.addFlashAttribute("result",texts);
    	if(texts.isEmpty() || texts==null)
    		redirectAttributes.addFlashAttribute("red", "There is no text for this criteria");
    	if(stForm.getTypesid().isEmpty() || stForm.getTypesid()==null)
    		redirectAttributes.addFlashAttribute("red", "You have to select at least one type");
    	
    	return "redirect:display?projectid="+projectid;
    }
    
    @RequestMapping(value="/display", method = RequestMethod.GET)
    public String displayText(@RequestParam int projectid, Model model, Principal user)
    {
    	SearchTextForm stf = new SearchTextForm();
    	List<Language> lan = this.languageService.getLanguagesByUsername(user.getName());
    	List<Type> types = this.typeService.getTypesByProjectID(projectid);
    	Project project = this.projectService.getProjectById(projectid);
    	Map<Integer,String> lansMap = new LinkedHashMap<Integer,String>();
    	User user1 = this.userService.getUserByUsername(user.getName());
    	Set<Language> lans = this.userService.getLanguages(user1);
    	Iterator iter = lans.iterator();
    	while(iter.hasNext())
    	{
    		Language l = (Language) iter.next();
    		lansMap.put(l.getLanguageid(), l.getName());
    	}
    	model.addAttribute("lans",lansMap);
    	stf.setProjectid(projectid);
    	model.addAttribute("searchform",stf);
    	model.addAttribute("languages", lan);
    	Map<Integer,String> typesMap = new LinkedHashMap<Integer,String>();
    	Set<Type> types1 = this.projectService.getTypes(projectid);
    	Iterator iterType = types1.iterator();
    	while(iterType.hasNext())
    	{
    		Type t = (Type) iterType.next();
    		typesMap.put(t.getTypeID(), t.getName());
    	}
    	model.addAttribute("types",typesMap);
    	//model.addAttribute("types", types);
    	model.addAttribute("project", project);
    	return "display";
    }
    
    @RequestMapping(value="/save")
    public String saveText(@ModelAttribute("textForm") @Valid TextForm tForm, BindingResult result, RedirectAttributes redirectAttributes, Principal user)
    {
    	if(result.hasErrors())
    	{
    		redirectAttributes.addFlashAttribute("error",result.getAllErrors().get(0).getDefaultMessage());
    		return "redirect:/";
    	}
    	Text text = new Text();
    	text.setLink(tForm.getLink());
    	text.setText(tForm.getText());
    	text.setTitle(tForm.getTitle());
    	Language lan = this.languageService.getLanguageById(tForm.getLanguageid());
    	text.setLanguage(lan);
    	Project project = this.projectService.getProjectById(tForm.getProjectid());
    	text.setProject(project);
    	List<Integer> typesid = tForm.getTypeids();
    	Set<Type> types = new HashSet<Type>();
    	Iterator<Integer> iter = typesid.iterator();
    	while(iter.hasNext())
    	{
    		int typeid = (int) iter.next();
    		Type type = this.typeService.getTypeById(typeid);
    		types.add(type);
    	}
    	text.setTypes(types);
    	Set<Word> words = new HashSet<Word>();
    	if(tForm.getWords()!=null)
    	{
    		List<Integer> wordsid = tForm.getWords();
    		Iterator<Integer> iterator = wordsid.iterator();
        	while(iterator.hasNext())
        	{
        		int wordid = (int) iterator.next();
        		Word word = this.wordService.getWordById(wordid);
        		words.add(word);
        	}
        	text.setWords(words);
    	}
    	this.textService.createText(project, lan, tForm.getTitle(), tForm.getText(), tForm.getLink(), words, types);
    	return "redirect:/insert?projectid="+tForm.getProjectid();
    }
}
	