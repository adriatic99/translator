package com.translator.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.translator.forms.TextForm;

public class TextFormValidator implements Validator {

	@Override
	public boolean supports(Class clazz) {
		return TextForm.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors e) {
		ValidationUtils.rejectIfEmpty(e, "title", "title.empty","Field Title cannot be empty");
		ValidationUtils.rejectIfEmpty(e, "text", "text.empty","Field Text cannot be empty");
		ValidationUtils.rejectIfEmpty(e, "typeids", "typeids.empty","You must select at least one type");
		ValidationUtils.rejectIfEmpty(e, "languageid", "typesid.empty","You must select at least one type");
		if(e.hasErrors())
			return;
		TextForm tf = (TextForm) obj;
		if(tf.getTitle().length()<2)
			e.rejectValue("title", "less than 2 characters");
		else if(tf.getText().length()<2)
			e.rejectValue("text", "text cannot be less than 2 characters");
	}

}
