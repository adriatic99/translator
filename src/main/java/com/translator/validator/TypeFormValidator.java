package com.translator.validator;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.core.io.Resource;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.translator.forms.LanForm;
import com.translator.forms.TextForm;
import com.translator.forms.TypeForm;

public class TypeFormValidator implements Validator {
    
	public TypeFormValidator() {}
	
	@Override
	public boolean supports(Class clazz) {
		return TypeForm.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors e) {
		
		ValidationUtils.rejectIfEmpty(e, "name", "name.required","The name field cannot be empty");
		TypeForm tf = (TypeForm) obj;
		if(!e.hasErrors())
		{
			if(tf.getName().length()<2)
				e.rejectValue("name", "name.incorrect","name is less than 2 characters");
		}
	}

}
