package com.translator.validator;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.core.io.Resource;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.translator.forms.LanForm;
import com.translator.forms.TextForm;

public class LanFormValidator implements Validator {
    
	public LanFormValidator() {}
	
	@Override
	public boolean supports(Class clazz) {
		return LanForm.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors e) {
		
		ValidationUtils.rejectIfEmpty(e, "name", "name.required","The uname cannot be empty");
		ValidationUtils.rejectIfEmpty(e, "code", "code.required");
		LanForm lf = (LanForm) obj;
		if(lf.getCode().length()!=2)
			e.rejectValue("code", "code.incorrect","code is not 2 characters");
	}

}
